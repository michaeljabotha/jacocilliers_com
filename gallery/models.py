from django.core.files import uploadedfile
from django.db import models
import os
import os.path
from PIL import Image as PImage
from io import BytesIO
from django.core.files.base import ContentFile

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=255, null=True)    

    def __str__(self):
        return self.name

    def get_images(self):
        return Image.objects.filter(category=self.id)

class Image(models.Model):
    name = models.CharField(max_length=255, null=True, default="image")
    image = models.ImageField()
    category = models.ManyToManyField(Category)  
    is_fullpage = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    thumbnail = models.ImageField(
        upload_to="thumbnails",
        max_length=500,
        null=True,
        blank=True
    )

    def save(self, *args, **kwargs):

        if not self.make_thumbnail():
            # set to a default thumbnail
            raise Exception('Could not create thumbnail - is the file type valid?')

        super(Image, self).save(*args, **kwargs)

    def make_thumbnail(self):

        image = PImage.open(self.image)
        image.thumbnail((900,900), PImage.ANTIALIAS)

        thumb_name, thumb_extension = os.path.splitext(self.image.name)
        thumb_extension = thumb_extension.lower()

        thumb_filename = thumb_name + '_thumb' + thumb_extension

        if thumb_extension in ['.jpg', '.jpeg']:
            FTYPE = 'JPEG'
        elif thumb_extension == '.gif':
            FTYPE = 'GIF'
        elif thumb_extension == '.png':
            FTYPE = 'PNG'
        else:
            return False    # Unrecognized file type

        # Save thumbnail to in-memory file as StringIO
        temp_thumb = BytesIO()
        image.save(temp_thumb, FTYPE)
        temp_thumb.seek(0)

        # set save=False, otherwise it will run in an infinite loop
        self.thumbnail.save(thumb_filename, ContentFile(temp_thumb.read()), save=False)
        temp_thumb.close()

        return True