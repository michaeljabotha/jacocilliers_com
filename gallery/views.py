from gallery.models import Category, Image
import os
import random
from django.shortcuts import render, get_object_or_404
from django.conf import settings

# Create your views here.
def chunker(list, chuck_size):
    return [list[i:i + chuck_size] for i in range(0, len(list), chuck_size)] 

def common(request):
    categories = Category.objects.all()

    data_json = {
        "categories": categories,
    }

    return data_json

def generic_view(request):
    images_qs = Image.objects.all()
    urls = []
    for image in images_qs:
        urls.append(image)
    
    random.shuffle(urls)

    # urls = chunker(urls, 20)

    data_json = {
        "images": urls,
    }
    return render(request, 'masonry.html', data_json)

def page_view(request):
    images_qs = Image.objects.filter(is_fullpage=True)
    urls = []
    for image in images_qs:
        urls.append(image)
    if urls == []:
        urls = ['']
    random.shuffle(urls)
    data_json = {
        "images": urls[0],
    }
    return render(request, 'fullpage.html', data_json)

def category_view(request, category_id):
    category = get_object_or_404(Category, id=category_id)
    images_qs = category.get_images()
    urls = []
    for image in images_qs:
        urls.append(image)
    
    random.shuffle(urls)
    # urls = chunker(urls, 20)
    data_json = {
        "images": urls,
    }
    return render(request, 'masonry.html', data_json)